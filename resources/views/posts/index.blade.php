@extends('layouts.master')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h4>
                    Post List
                    <span class="float-end">
                        <a class="btn btn-primary" href="{{  route("posts.create") }}">New Post</a>
                    </span>
                </h4>
                <hr>
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Body</th>
                        <th scope="col">Status</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Action</th>

                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <td>@mdo</td>
                        <td>
                            <a href="{{ route("posts.show", 1) }}" class="btn btn-success">View</a>
                            <a href="" class="btn btn-danger">Delete</a>
                            <a href="{{  route("posts.edit", 1) }}" class="btn btn-info">Edit</a>
                        </td>
                      </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
@endsection
