@extends('layouts.master')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12 mb-3">
        <div class="card">
            <div class="card-body">
                <h3>
                    Post Title
                    <span class="float-end">
                        <a href="{{ route("posts.index") }}" class="btn btn-info">Back</a>
                    </span>
                </h3>
                <hr>
                <span class="text-danger">Author by: Mr David</span>
                <br>
                {{-- <small>
                    @foreach ($post->tags as $tag)
                        <span class="badge badge-primary">{{  $tag->name }}</span>
                    @endforeach
                </small> --}}
                <section>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis ipsum corrupti, magnam placeat atque eos nostrum cum sunt fugit! Accusamus corporis voluptate qui nesciunt suscipit repellendus? Debitis eaque corporis quidem?
                </section>
            </div>
        </div>
    </div>

    <div class="col-md-12">

        <div class="card mt-3">
            <div class="card-body">
                <form action="" method="post">
                    @csrf
                    <div class="form-group">
                      <label>Comment Here</label>
                      <textarea name="comment" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mt-2">Add Comment</button>
                </form>
            </div>
        </div>


            <div class="card mt-3">
                <div class="card-body">
                    Ali said at 5 hours ago <br>
                    <hr>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Id vel autem nulla explicabo rem? Similique, tenetur! Aspernatur tempore sunt cupiditate quae, atque est omnis, dolores ullam sint odio corrupti ad.
                </div>
            </div>

    </div>

</div>
@endsection
